package actors

import actors.messages.Stop
import akka.actor.Actor
import akka.stream.ActorFlowMaterializer
import akka.stream.scaladsl.{Sink, Source}
import elasticsearch.EsWriter
import flow.FlowFactory
import io.scalac.amqp.{Address, Connection, ConnectionSettings}
import org.apache.logging.log4j.LogManager
import processors.SentimentProcessor._
import scala.collection.immutable.Seq
import scala.concurrent.duration.{Duration, _}
import scala.language.postfixOps

class NodeWorker(host: String, port: Int, username: String, password: String, queue: String) extends Actor with FlowFactory {

  val logger = LogManager.getLogger("Node worker")

  override def preStart() = {
    implicit val materializer = ActorFlowMaterializer()
    val settings = ConnectionSettings(Seq(Address(host, port)), "/", username, password, None, Duration.Inf, 5 seconds)
    val connection = Connection(settings)
    val rabbitConsumer = Source(connection.consume(queue = queue))
    val pipeline = rabbitConsumer via
      consumerMapping via
      messageTransformer
      Sink.foreach(s => {
        EsWriter.write(s)
      })
    pipeline run()
    logger.info("Start consuming")
  }

  override def receive: Receive = {
    case Stop =>
      logger.info("Received stop signal")
      context.stop(self)
    case _ =>
  }
}
