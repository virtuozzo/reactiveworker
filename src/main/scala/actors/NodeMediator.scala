package actors


import actors.messages.{Connect, Register, Stop}
import akka.actor.{Actor, Props}
import com.typesafe.config.ConfigFactory
import org.apache.logging.log4j.LogManager

class NodeMediator extends Actor {

  val logger = LogManager.getLogger("Node manager")

  override def preStart() = {
    val conf = ConfigFactory.load()
    val host = conf.getString("orchestrator.host")
    val port = conf.getInt("orchestrator.port")
    context.actorSelection(s"akka.tcp://ReactiveSystem@$host:$port/user/orchestrator") ! Register
  }

  override def receive: Receive = {
    case Connect(host, port, username, password, queue) =>
      logger.info("Received connect command")
      context.actorOf(Props(new NodeWorker(host, port, username, password, queue)), "worker")

    case Stop =>
      context.children.foreach(child => child ! Stop)

    case _ =>
  }
}
