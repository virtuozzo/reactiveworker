package actors.messages

final case class Connect(host: String,
                                   port: Int,
                                   username: String,
                                   password: String,
                                   queue: String)