package processors

import java.util.Properties

import _root_.edu.stanford.nlp.ling.CoreAnnotations
import _root_.edu.stanford.nlp.neural.rnn.RNNCoreAnnotations
import _root_.edu.stanford.nlp.sentiment.SentimentCoreAnnotations
import akka.stream.scaladsl.Flow
import edu.stanford.nlp.pipeline.StanfordCoreNLP
import model.SentimentCategory.SentimentCategory
import model.{GradedMessage, SentimentCategory}

object SentimentProcessor {

  val props = new Properties()
  props.setProperty("annotators", "tokenize, ssplit, parse, sentiment");
  val pipeline = new StanfordCoreNLP(props)


  def messageTransformer: Flow[String, GradedMessage, Unit] = Flow[String].map(text =>
    GradedMessage(text , getGrade(text).toString))

  def getGrade(msg : String): SentimentCategory = {
    var mainSentiment = 0
    if (msg != null && msg.length() > 0) {
      var longest = 0
      val annotation = pipeline.process(msg)
      val list = annotation.get(classOf[CoreAnnotations.SentencesAnnotation])
      val it = list.iterator()
      while (it.hasNext)
      {
        val sentence = it.next()
        val tree = sentence.get(classOf[SentimentCoreAnnotations.AnnotatedTree])
        val sentiment = RNNCoreAnnotations.getPredictedClass(tree)
        val partText = sentence.toString()
        if (partText.length() > longest) {
          mainSentiment = sentiment
          longest = partText.length()
        }
      }
    }
    import SentimentCategory._
    if (mainSentiment < 2)
      Negative
    else if (mainSentiment == 2)
      Neutral
    else
      Positive
  }



}
