package processors

import java.util.Date

import akka.stream.scaladsl.Flow
import model.LogMessage

object LogProcessor {

  val logPattern = "(.*?)\\s(INFO|WARN|ERROR)\\s+?\\[(.*?)\\]\\s+?\\((.*?)\\)(.*)$".r

  def logTransformer: Flow[String, Option[LogMessage], Unit] = Flow[String].map {
    case logPattern(time, level, classPath, thread, message) =>
      Thread.sleep(20)
      Some(LogMessage(new Date(), level, classPath, thread, message))
    case _ => None
  }

  def logFilter: Flow[Option[LogMessage], LogMessage, Unit] = Flow[Option[LogMessage]].filter(_.isDefined).map(_.get)




}
