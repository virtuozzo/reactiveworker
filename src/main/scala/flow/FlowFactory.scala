package flow

import akka.stream.scaladsl.Flow
import io.scalac.amqp.Delivery

trait FlowFactory {


  def consumerMapping: Flow[Delivery, String, Unit] =
    Flow[Delivery]
      .map(_.message.body.utf8String)
}
