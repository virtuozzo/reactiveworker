package model

case class GradedMessage(text: String, grade: String)
