package elasticsearch

import java.net.InetAddress
import java.util.Date

import com.sksamuel.elastic4s.ElasticClient
import com.sksamuel.elastic4s.ElasticDsl._
import com.sksamuel.elastic4s.source.ObjectSource
import model.LogMessage

object EsWriter {

  val host = InetAddress.getLocalHost.getHostName

  lazy val client = {
    val client = ElasticClient.remote("46.101.151.182", 9300)
    client.execute {
      create index "logs" mappings (
        mapping name "time" timestamp true
        )
    }
    client
  }

  def write(log: LogMessage) = {
    client.execute {
      index into "logs" doc ObjectSource(log)
    }
  }


  def log(msg: String) = {
    client.execute {
      index into "workers" fields {
        "time" -> new Date().toString
        "host" -> host
        "message" -> msg
      }
    }
  }

}
