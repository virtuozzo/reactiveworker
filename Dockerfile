FROM java:7
MAINTAINER Dmitry Kostyaev <dmitry@kostyaev.me>
ADD target/scala-2.11/ReactiveWorker-assembly-0.1.jar /opt/ReactiveWorker/
WORKDIR /opt/ReactiveWorker
CMD ["java", "-Djava.security.egd=file:/dev/./urandom", "-jar", "ReactiveWorker-assembly-0.1.jar"]