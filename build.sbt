name := "ReactiveWorker"

version := "0.1"

scalaVersion := "2.11.6"

libraryDependencies ++= Seq(
  "com.typesafe.akka"             %%    "akka-actor"                              %   "2.3.11",
  "com.typesafe.akka"             %%    "akka-remote"                             %   "2.3.11",
  "com.typesafe.akka"             %     "akka-stream-experimental_2.11"           %   "1.0-M5",
  "com.sksamuel.elastic4s"        %%    "elastic4s"                               %   "1.5.10",
  "com.fasterxml.jackson.core"    %     "jackson-core"                            %   "2.4.2",
  "com.fasterxml.jackson.core"    %     "jackson-databind"                        %   "2.4.2",
  "com.fasterxml.jackson.module"  %%    "jackson-module-scala"                    %   "2.4.2",
  "io.scalac"                     %%    "reactive-rabbit"                         %   "0.2.2",
  "com.typesafe"                  %     "config"                                  %   "1.2.1",
  "org.apache.logging.log4j"      %     "log4j-core"                              %   "2.3",
  "org.apache.logging.log4j"      %     "log4j-api"                               %   "2.3",
  "edu.stanford.nlp" % "stanford-corenlp" % "3.3.1" artifacts (Artifact("stanford-corenlp", "models"), Artifact("stanford-corenlp"))


)

assemblyMergeStrategy in assembly := {
  case PathList(ps @ _*) if ps.last endsWith "ToString.class" => MergeStrategy.first
  case PathList(ps @ _*) if ps.last endsWith "FromString.class" => MergeStrategy.first
  case other => MergeStrategy.defaultMergeStrategy(other)
}